val rdd = sc.textFile("100.txt")
var lineCount=155213

////

val pairs = rdd.map{ 

    _.split('.').map{ substrings =>

        substrings.trim.split(' ').

		map{_.toLowerCase.replaceAll("""[^\p{IsAlphabetic}]+""","")}.

        sliding(2)
    }.
    flatMap{identity}.map{_.mkString(" ")}.

    groupBy{identity}.mapValues{_.size}

}.
flatMap{identity}.
reduceByKey(_+_).
sortByKey()

val total_words = rdd.
flatMap(line => line.toLowerCase.replaceAll("(^[^a-z]+|[^a-z]+$)",	"").split(" "))

val wordCnts = total_words.
map(x => (x, 1)).reduceByKey(_+_).sortByKey()

for((x,y) <- pairs)
{
	val keys=x.split(" ")
	val cntX=wordCnts.lookup(keys(0))
	val px=cntX(0) / lineCount
	
	val cntY=wordCnts.lookup(keys(1))
	val py=cntY(1) / lineCount
	
	var pxy =(y/lineCount) 
	
	//pmi=log(pxy/(px*py))
	
	var pmi=scala.math.log(px*py)/scala.math.log(pxy) -1
	
	pmi -> y 
}
//save
pairs.saveAsTextFile("output_1")